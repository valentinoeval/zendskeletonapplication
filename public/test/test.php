<?php
	class a {
		public function a() {
			echo 'Je suis dans a \n';
		}
		protected function m1() {
			echo 'Je suis dans a m1 \n';
		}
	}
	class b extends a {
		protected function m() {
			parent::m1();
			echo 'Je suis dans a \n';
		}
		protected function m1() {
			m();
		}
	}
	$obj=new b();
	$obj->m1();