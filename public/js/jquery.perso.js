$(document).ready(function() {
	//click bouton modification fiche client, récupère les valeurs de la liste de la fiche client et les réinjecte dans les champs du formulaire
	$('.modify_fiche').click(function() {
		$('#form_id_customer').val($('#id_customer').text());
		$('#form_firstname').val($('#firstname').text());
		$('#form_lastname').val($('#lastname').text());
		$('#form_email').val($('#email').text());
		$('#form_company_name').val($('#company_name').text());
		$('#form_phone').val($('#phone').text());
		$('#form_company_adress').val($('#company_adress').text());
		$('#form_company_city').val($('#company_city').text());
		$('#form_company_department').val($('#company_department').text());
	});

	//affichage notes masquées
	$('.show_note').click(function() {
		$('.hidden_note').slideDown();
	});

	//confirmation avant la suppression d'une tâche
	$('.confirm-del-customer').click(function() {
		//incrustation du lien complété avec le bon identifiant pour l'overlayer
		$("#del-link").attr("href", "/delTask/"+this.id+"/customer");
	});
	$('.confirm-del-fiche').click(function() {
		var id_customer=$('.row').attr('id');
		//incrustation du lien complété avec le bon identifiant pour l'overlayer
		$("#del-link").attr("href", "/delTask/"+this.id+"/fiche/"+id_customer);
	});

	//modifier le titre et le bouton submit de l'overlayer de modification d'une fiche client
	$('.modify_fiche').click(function() {
		$('.title-fiche').text('Modifier un client');
		$('#submitCustomer').attr('value', 'Modifier');
	});
});