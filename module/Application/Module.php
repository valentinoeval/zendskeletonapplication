<?php
	/**
	 * Zend Framework (http://framework.zend.com/)
	 *
	 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
	 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
	 * @license   http://framework.zend.com/license/new-bsd New BSD License
	 */

	namespace Application;

	use Zend\Mvc\ModuleRouteListener;
	use Zend\Mvc\MvcEvent;
	use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
	use Zend\Authentication\Storage;
	use Zend\Authentication\AuthenticationService;
	use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;
	use Zend\Db\Adapter\Adapter;
	use Zend\EventManager\EventInterface;
	use Application\Model\CustomerTable;
	use Application\Model\LoginTable;
	use Application\Model\TaskTable;
	use Application\Model\NoteTable;

	class Module implements AutoloaderProviderInterface {
		public function onBootstrap(MvcEvent $e) {
			$eventManager        = $e->getApplication()->getEventManager();
			$moduleRouteListener = new ModuleRouteListener();
			$moduleRouteListener->attach($eventManager);
		}

		public function getConfig() {
			return include __DIR__ . '/config/module.config.php';
		}

		public function getAutoloaderConfig() {
			return array(
				'Zend\Loader\ClassMapAutoloader' => array(
					__DIR__ . '/autoload_classmap.php'
				),
				'Zend\Loader\StandardAutoloader' => array(
					'namespaces' => array(
						__NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
					),
				),
			);
		}

		public function getServiceConfig() {
			return array(
				'factories' => array(
					'Application\Model\CustomerTable' => function($sm) {
						$dbAdapter  = $sm->get('Zend\Db\Adapter\Adapter');
						$table 		= new CustomerTable($dbAdapter);

						return $table;
					},
					'Application\Model\LoginTable' => function($sm) {
						$dbAdapter  = $sm->get('Zend\Db\Adapter\Adapter');
						$table 		= new LoginTable($dbAdapter);

						return $table;
					},
					'Application\Model\TaskTable' => function($sm) {
						$dbAdapter  = $sm->get('Zend\Db\Adapter\Adapter');
						$table 		= new TaskTable($dbAdapter);

						return $table;
					},
					'Application\Model\NoteTable' => function($sm) {
						$dbAdapter  = $sm->get('Zend\Db\Adapter\Adapter');
						$table 		= new NoteTable($dbAdapter);

						return $table;
					}
				)
			);
		}
	}