<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
	'router' => array(
		'routes' => array(
			'home' => array(
				'type' => 'Zend\Mvc\Router\Http\Literal',
				'options' => array(
					'route'    => '/',
					'defaults' => array(
						'controller' => 'Application\Controller\Index',
						'action'     => 'index',
					),
				),
			),
			'login' => array(
				'type'    => 'Literal',
				'options' => array(
					'route'    => '/login',
					'defaults' => array(
						'controller'    => 'Application\Controller\Login',
						'action'        => 'login',
					),
				),
				'may_terminate' => true,
				'child_routes' => array(
					'default' => array(
						'type'    => 'Segment',
						'options' => array(
							'route'    => '/[:controller[/:action]]',
							'constraints' => array(
								'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
								'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
							),
							'defaults' => array(
							),
						),
					),
				),
			),
			'logout' => array(
				'type'    => 'Literal',
				'options' => array(
					'route'    => '/logout',
					'defaults' => array(
						'controller' => 'Application\Controller\Login',
						'action'     => 'logout'
					)
				)
			),
			'customer' => array(
				'type'		=> 'segment',
				'options'	=> array(
					'route'    => '/customer[/page/:page]',
					'constraints' => array(
						'page'	=> '[0-9]+'
					),
					'defaults' => array(
						'controller'    => 'Application\Controller\Customer',
						'action'        => 'customer',
					),
				),
				'may_terminate' => true,
				'child_routes' => array(
					'default' => array(
						'type'    => 'Segment',
						'options' => array(
							'route'    => '/[:controller[/:action]]',
							'constraints' => array(
								'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
								'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
							),
							'defaults' => array(
							)
						)
					)
				)
			),
			'fiche' => array(
				'type'    => 'segment',
				'options' => array(
					'route'    => '/fiche[/:action][/:id_customer]',
					'constraints' => array(
						'action'		=> '[a-zA-Z][a-zA-Z0-9_-]*',
						'id_customer'	=> '[0-9]+'
					),
					'defaults' => array(
						'controller' => 'Application\Controller\Fiche',
						'action'     => 'fiche'
					)
				)
			),
			'todoTask' => array(
				'type'    => 'segment',
				'options' => array(
					'route'    => '/todoTask[/:id_task][/:route][/:id_customer]',
					'constraints' => array(
						'id_task'		=> '[0-9]+',
						'route'			=> '[a-zA-Z][a-zA-Z0-9_-]*',
						'id_customer'	=> '[0-9]+'
					),
					'defaults' => array(
						'controller' => 'Application\Controller\Fiche',
						'action'     => 'todoTask'
					)
				)
			),
			'delTask' => array(
				'type'    => 'segment',
				'options' => array(
					'route'    => '/delTask[/:id_task][/:route][/:id_customer]',
					'constraints' => array(
						'id_task'		=> '[0-9]+',
						'route'			=> '[a-zA-Z][a-zA-Z0-9_-]*',
						'id_customer'	=> '[0-9]+'
					),
					'defaults' => array(
						'controller' => 'Application\Controller\Fiche',
						'action'     => 'delTask'
					)
				)
			),
		),
	),
	'service_manager' => array(
		'abstract_factories' => array(
			'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
			'Zend\Log\LoggerAbstractServiceFactory',
		),
		'aliases' => array(
			'translator' => 'MvcTranslator',
		),
	),
	'translator' => array(
		'locale' => 'fr_FR',
		'translation_file_patterns' => array(
			array(
				'type'     => 'gettext',
				'base_dir' => __DIR__ . '/../language',
				'pattern'  => '%s.mo',
			),
		),
	),
	'controllers' => array(
		'invokables' => array(
			'Application\Controller\Index'		=> 'Application\Controller\IndexController',
			'Application\Controller\Customer'	=> 'Application\Controller\CustomerController',
			'Application\Controller\Login'		=> 'Application\Controller\LoginController',
			'Application\Controller\Fiche'		=> 'Application\Controller\FicheController'
		),
	),
	'view_manager' => array(
		'display_not_found_reason' => true,
		'display_exceptions'       => true,
		'doctype'                  => 'HTML5',
		'not_found_template'       => 'error/404',
		'exception_template'       => 'error/index',
		'template_map' => array(
			'layout/layout'           	=> __DIR__ . '/../view/layout/layout.phtml',
			'layout/layout-login'      	=> __DIR__ . '/../view/layout/layout-login.phtml',
			'application/index/index' 	=> __DIR__ . '/../view/application/index/index.phtml',
			'error/404'               	=> __DIR__ . '/../view/error/404.phtml',
			'error/index'             	=> __DIR__ . '/../view/error/index.phtml',
			'paginator'					=> __DIR__ . '/../view/layout/paginator.phtml'
		),
		'template_path_stack' => array(
			'Application' => __DIR__ . '/../view',
		)
	),
	// Placeholder for console routes
	'console' => array(
		'router' => array(
			'routes' => array(
			),
		),
	)
);