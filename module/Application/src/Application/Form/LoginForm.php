<?php
	namespace Application\Form;
	
	use Zend\Form\Form;
	
	class LoginForm extends Form {
		public function __construct() {
			parent::__construct();
			$this->setAttribute('method', 'post');
			$this->setAttribute('class', 'form-signin');
			$this->add(
				array(
					'name'	=> 'login',
					'type'	=> 'text',
					'attributes' => array(
						'placeholder' 	=> 'Login',
						'class'			=> 'form-control'
					)
				)
			);
			$this->add(
				array(
					'name'	=> 'password',
					'type'	=> 'password',
					'attributes' => array(
						'placeholder' 	=> 'Mot de passe',
						'class'			=> 'form-control'
					)
				)
			);
			$this->add(
				array(
					'name'	=> 'submit',
					'type'	=> 'Submit',
					'attributes'=> array(
						'value'	=> 'Connexion',
						'id'	=> 'submitButton',
						'class'	=> 'btn btn-default'
					)
				)
			);
		}
	}