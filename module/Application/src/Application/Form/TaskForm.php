<?php
	namespace Application\Form;
	
	use Zend\Form\Form;
	
	class TaskForm extends Form {
		public function __construct() {
			parent::__construct();
			$this->setAttribute('method', 'post');
			$this->setAttribute('action', 'addTask');
			$this->add(
				array(
					'name'	=> 'id_task',
					'type'	=> 'hidden'
				)
			);
			$this->add(
				array(
					'name'	=> 'content',
					'type'	=> 'textarea',
					'attributes' => array(
						'placeholder' 	=> 'Contenu tâche ...',
						'class'			=> 'form-control'
					)
				)
			);
			$this->add(
				array(
					'name'	=> 'date',
					'type'	=> 'date',
					'attributes' => array(
						'class'			=> 'form-control'
					)
				)
			);
			$this->add(
				array(
					'name'	=> 'todo',
					'type'	=> 'hidden',
					'attributes' => array(
						'value' => '1'
					)
				)
			);
			$this->add(
				array(
					'name'	=> 'submit',
					'type'	=> 'submit',
					'attributes' => array(
						'value' => 'Enregistrer',
						'id'	=> 'submitButton',
						'class'	=> 'btn btn-primary',
						'type'	=> 'button'
					)
				)
			);
		}
	}