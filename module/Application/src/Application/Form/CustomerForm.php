<?php
	namespace Application\Form;
	
	use Zend\Form\Form;
	
	class CustomerForm extends Form {
		public function __construct() {
			parent::__construct();
			$this->setAttribute('method', 'post');
			$this->add(
				array(
					'name'	=> 'id_customer',
					'type'	=> 'hidden',
					'attributes' => array(
						'id'			=> 'form_id_customer'
					)
				)
			);
			$this->add(
				array(
					'name'	=> 'firstname',
					'type'	=> 'text',
					'attributes' => array(
						'placeholder' 	=> 'Prénom',
						'class'			=> 'form-control',
						'id'			=> 'form_firstname'
					)
				)
			);
			$this->add(
				array(
					'name'	=> 'lastname',
					'type'	=> 'text',
					'attributes' => array(
						'placeholder' 	=> 'Nom',
						'class'			=> 'form-control',
						'id'			=> 'form_lastname'
					)
				)
			);
			$this->add(
				array(
					'name'	=> 'email',
					'type'	=> 'text',
					'attributes' => array(
						'placeholder' 	=> 'Email',
						'class'			=> 'form-control',
						'id'			=> 'form_email'
					)
				)
			);
			$this->add(
				array(
					'name'	=> 'company_name',
					'type'	=> 'text',
					'attributes' => array(
						'placeholder' 	=> 'Entreprise',
						'class'			=> 'form-control',
						'id'			=> 'form_company_name'
					)
				)
			);
			$this->add(
				array(
					'name'	=> 'phone',
					'type'	=> 'text',
					'attributes' => array(
						'placeholder' 	=> 'Téléphone',
						'class'			=> 'form-control',
						'id'			=> 'form_phone'
					)
				)
			);
			$this->add(
				array(
					'name'	=> 'company_adress',
					'type'	=> 'text',
					'attributes' => array(
						'placeholder' 	=> 'Adresse entreprise',
						'class'			=> 'form-control',
						'id'			=> 'form_company_adress'
					)
				)
			);
			$this->add(
				array(
					'name'	=> 'company_city',
					'type'	=> 'text',
					'attributes' => array(
						'placeholder' 	=> 'Ville entreprise',
						'class'			=> 'form-control',
						'id'			=> 'form_company_city'
					)
				)
			);
			$this->add(
				array(
					'name'	=> 'company_department',
					'type'	=> 'text',
					'attributes' => array(
						'placeholder' 	=> 'Département entreprise',
						'class'			=> 'form-control',
						'id'			=> 'form_company_department'
					)
				)
			);
			$this->add(
				array(
					'name'	=> 'submit',
					'type'	=> 'Submit',
					'attributes'=> array(
						'value'			=> 'Ajouter',
						'id'			=> 'submitCustomer',
						'class'			=> 'btn btn-primary',
						'type'			=> 'button'
					)
				)
			);
		}
	}