<?php
	namespace Application\Form;
	
	use Zend\Form\Form;
	
	class NoteForm extends Form {
		public function __construct() {
			parent::__construct();
			$this->setAttribute('method', 'post');
			$this->setAttribute('action', 'addNote');
			$this->add(
				array(
					'name'	=> 'id_note',
					'type'	=> 'hidden'
				)
			);
			$this->add(
				array(
					'name'	=> 'content',
					'type'	=> 'textarea',
					'attributes' => array(
						'placeholder' 	=> 'Contenu note ...',
						'class'			=> 'form-control'
					)
				)
			);
			$this->add(
				array(
					'name'	=> 'submit',
					'type'	=> 'submit',
					'attributes' => array(
						'value' => 'Enregistrer',
						'id'	=> 'submitButton',
						'class'	=> 'btn btn-primary',
						'type'	=> 'button'
					)
				)
			);
		}
	}