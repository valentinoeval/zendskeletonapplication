<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

	namespace Application\Controller;

	use Zend\Mvc\Controller\AbstractActionController;
	use Zend\View\Model\ViewModel;
	use Zend\Session\Container;
	use Zend\Session\SessionManager;

	class IndexController extends AbstractActionController {
		public function indexAction() {
			$sessionManager=new SessionManager();
			$sessionManager->start();
			$userContainer=new Container('user');
			if (!$userContainer->offsetExists('islogged') or $userContainer->islogged!=true)
				$this->layout('layout/layout-login');
			else
				$this->layout('layout/layout');

			return new ViewModel();
		}
	}