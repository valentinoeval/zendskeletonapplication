<?php
	namespace Application\Controller;

	use Zend\Mvc\Controller\AbstractActionController;
	use Zend\View\Model\ViewModel;
	use Application\Model\CustomerTable;
	use Application\Model\Customer;
	use Application\Form\CustomerForm;
	use Application\Model\CustomerFormFilter;
	use Application\Model\TaskTable;
	use Application\Model\Task;
	use Application\Form\TaskForm;
	use Application\Model\TaskFormFilter;
	use Application\Model\NoteTable;
	use Application\Model\Note;
	use Application\Form\NoteForm;
	use Application\Model\NoteFormFilter;
	use Zend\Session\Container;
	use Zend\Session\SessionManager;
	use Zend\Debug\Debug;
	use Zend\Paginator\Paginator;
	use Zend\Paginator\Adapter\Iterator as paginatorIterator;

	class FicheController extends AbstractActionController {
		protected $CustomerTable;
		protected $TaskTable;
		protected $NoteTable;

		public function getCustomerTable() {
			if (!$this->CustomerTable) {
				$sm=$this->getServiceLocator();
				$this->CustomerTable=$sm->get('Application\Model\CustomerTable');
			}
			return $this->CustomerTable;
		}

		public function getTaskTable() {
			if (!$this->TaskTable) {
				$sm=$this->getServiceLocator();
				$this->TaskTable=$sm->get('Application\Model\TaskTable');
			}
			return $this->TaskTable;
		}

		public function getNoteTable() {
			if (!$this->NoteTable) {
				$sm=$this->getServiceLocator();
				$this->NoteTable=$sm->get('Application\Model\NoteTable');
			}
			return $this->NoteTable;
		}

		public function ficheAction() {
			$sessionManager=new SessionManager();
			$sessionManager->start();
			$userContainer=new Container('user');
			if (!$userContainer->offsetExists('islogged') or $userContainer->islogged!=true)
				$this->redirect()->toRoute('login');
			
			$view=new ViewModel();
			$id_customer=$this->params()->fromRoute('id_customer');
			$page=$this->params()->fromRoute('page')?(int)$this->params()->fromRoute('page'):1;
			
			$list_customers=$this->getCustomerTable()->fetchAll();
			$itemsPerPage=10;
			$paginator=new Paginator(new paginatorIterator($list_customers));
			$paginator->setCurrentPageNumber($page)
					->setItemCountPerPage($itemsPerPage)
					->setPageRange(3);
			$list_tasks=$this->getTaskTable()->getTaskByCustomer($id_customer);
			$infos_customer=$this->getCustomerTable()->getCustomer($id_customer);
			$list_notes=$this->getNoteTable()->getNoteByCustomer($id_customer);

			$form_customer=new CustomerForm();
			$customerFormFilter=new CustomerFormFilter();
			$form_customer->setInputFilter($customerFormFilter);

			//vérification de l'entrée des données POST
			if ($this->getRequest()->isPost()) {
				$form_customer->setData($_POST);
				if ($form_customer->isValid()) {
					$postData=new Customer();
					$postData->id_customer			= $_POST['id_customer'];
					$postData->firstname			= $_POST['firstname'];
					$postData->lastname				= $_POST['lastname'];
					$postData->email 				= $_POST['email'];
					$postData->company_name			= $_POST['company_name'];
					$postData->phone				= $_POST['phone'];
					$postData->company_adress		= $_POST['company_adress'];
					$postData->company_city			= $_POST['company_city'];
					$postData->company_department	= $_POST['company_department'];

					$this->getCustomerTable()->saveCustomer($postData);
					$this->redirect()->toRoute('fiche', array('action' => 'fiche', 'id_customer' => $id_customer));
				}
			}

			$form_task=new TaskForm();
			//ajout d'un champ contenant comme value l'identifiant du client associé
			$form_task->add(array(
				'name'	=> 'id_customer',
				'type'	=> 'hidden',
				'attributes' => array(
					'value' => $id_customer
				)
			));
			$taskFormFilter=new TaskFormFilter();
			$form_task->setInputFilter($taskFormFilter);

			$form_note=new NoteForm();
			//ajout d'un champ contenant comme value l'identifiant du client associé
			$form_note->add(array(
				'name'	=> 'id_customer',
				'type'	=> 'hidden',
				'attributes' => array(
					'value' => $id_customer
				)
			));
			$noteFormFilter=new NoteFormFilter();
			$form_note->setInputFilter($noteFormFilter);

			return $view->setVariables(array(
				'list_customers' 	=> $list_customers,
				'list_tasks' 		=> $list_tasks,
				'list_notes'		=> $list_notes,
				'paginator'			=> $paginator,
				'form_customer' 	=> $form_customer,
				'form_task'			=> $form_task,
				'form_note'			=> $form_note,
				'infos_customer' 	=> $infos_customer,
				'id_customer'		=> $id_customer
			));
		}

		public function addTaskAction() {
			$sessionManager=new SessionManager();
			$sessionManager->start();
			$userContainer=new Container('user');
			if (!$userContainer->offsetExists('islogged') or $userContainer->islogged!=true)
				$this->redirect()->toRoute('login');

			$postData=new Task();
			$postData->id_task=$_POST['id_task'];
			$postData->id_customer=$_POST['id_customer'];
			$postData->content=nl2br($_POST['content']);
			$postData->exec_date=$_POST['date'];
			$postData->todo=$_POST['todo'];
			$this->getTaskTable()->saveTask($postData);

			return $this->redirect()->toRoute('fiche', array('id_customer' => $_POST['id_customer']));
		}

		public function todoTaskAction() {
			$sessionManager=new SessionManager();
			$sessionManager->start();
			$userContainer=new Container('user');
			if (!$userContainer->offsetExists('islogged') or $userContainer->islogged!=true)
				$this->redirect()->toRoute('login');

			$id_task=$this->params()->fromRoute('id_task');
			$route=$this->params()->fromRoute('route');
			$id_customer=$this->params()->fromRoute('id_customer');

			$row=$this->getTaskTable()->getTask($id_task);
			foreach ($row as $select) {
				$task=new Task();
				$task->id_task=$select->id_task;
				$task->id_customer=$select->id_customer;
				$task->content=$select->content;
				$task->exec_date=$select->exec_date;
				$task->todo=0;
				$this->getTaskTable()->saveTask($task);
			}

			if ($route=='fiche')
				return $this->redirect()->toRoute($route, array('action' => 'fiche', 'id_customer' => $id_customer));
			else
				return $this->redirect()->toRoute($route);
			
		}

		public function delTaskAction() {
			$sessionManager=new SessionManager();
			$sessionManager->start();
			$userContainer=new Container('user');
			if (!$userContainer->offsetExists('islogged') or $userContainer->islogged!=true)
				$this->redirect()->toRoute('login');

			$id_task=$this->params()->fromRoute('id_task');
			$route=$this->params()->fromRoute('route');
			$id_customer=$this->params()->fromRoute('id_customer');
			$this->getTaskTable()->deleteTask($id_task);

			if ($route=='fiche')
				return $this->redirect()->toRoute($route, array('action' => 'fiche', 'id_customer' => $id_customer));
			else
				return $this->redirect()->toRoute($route); 
		}

		public function addNoteAction() {
			$sessionManager=new SessionManager();
			$sessionManager->start();
			$userContainer=new Container('user');
			if (!$userContainer->offsetExists('islogged') or $userContainer->islogged!=true)
				$this->redirect()->toRoute('login');
			
			$postData=new Note();
			$postData->id_note=$_POST['id_note'];
			$postData->id_customer=$_POST['id_customer'];
			$postData->content=nl2br($_POST['content']);
			$this->getNoteTable()->saveNote($postData);

			return $this->redirect()->toRoute('fiche', array('id_customer' => $_POST['id_customer']));
		}
	}