<?php

	namespace Application\Controller;

	use Zend\Mvc\Controller\AbstractActionController;
	use Zend\View\Model\ViewModel;
	use Application\Model\LoginTable;
	use Application\Model\Login;
	use Application\Form\LoginForm;
	use Application\Model\LoginFormFilter;
	use Zend\Session\Container;
	use Zend\Session\SessionManager;
	use Zend\Debug\Debug;

	class LoginController extends AbstractActionController {
		protected $LoginTable;

		public function getLoginTable() {
			if (!$this->LoginTable) {
				$sm=$this->getServiceLocator();
				$this->LoginTable=$sm->get('Application\Model\LoginTable');
			}
			return $this->LoginTable;
		}

		public function loginAction() {
			//instanciation du session manager
			$sessionManager=new SessionManager();
			$sessionManager->start();
			//instanciation de la session
			$userContainer=new Container('user');
			if ($userContainer->offsetExists('islogged') or $userContainer->islogged==true)
				$this->redirect()->toRoute('customer');

			$view=new ViewModel();
			$this->layout('layout/layout-login');
			$formLog=new LoginForm();
			$formFilter=new LoginFormFilter();
			$formLog->setInputFilter($formFilter);
			$message=null;
			if ($this->getRequest()->isPost()) {
				$formLog->setData($_POST);
				if ($formLog->isValid()) {
					$login=$_POST['login'];
					$hash=sha1($_POST['password']);
					$result=$this->getLoginTable()->getLogin($login);
					$user=null;
					foreach ($result as $row) {$user=$row;}
					if ($user!=null) {
						if ($user->login==$login) {
							if ($user->password==$hash) {
								$userContainer->islogged=true;
								$userContainer->login=$_POST['login'];
								if ($userContainer->offsetExists('islogged') and $userContainer->islogged==true) {
									$this->layout('layout/layout');
									$view->setTemplate('application/login/success.phtml');
									return $view->setVariables(array('login' => $user->login));
								}
							}
							else
								$message='Mot de passe erroné';
						}
						else
							$message='Cet utilisateur n\'existe pas';
					}
					else
						$message='Cet utilisateur n\'existe pas';
				}
			}
			return $view->setVariables(array('formLog' => $formLog, 'error' => $message));
		}
		
		/**
		 *	Function to logout the CRM user
		 */
		public function logoutAction() {
			$sessionManager=new SessionManager();
			$sessionManager->start();
			$userContainer=new Container('user');
			$userContainer->getManager()->getStorage()->clear('user');
			
			return $this->redirect()->toRoute('login');
		}
	}