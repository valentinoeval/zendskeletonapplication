<?php
	namespace Application\Controller;

	use Zend\Mvc\Controller\AbstractActionController;
	use Zend\View\Model\ViewModel;
	use Application\Model\CustomerTable;
	use Application\Model\Customer;
	use Application\Form\CustomerForm;
	use Application\Model\CustomerFormFilter;
	use Application\Model\TaskTable;
	use Application\Model\Task;
	use Zend\Session\Container;
	use Zend\Session\SessionManager;
	use Zend\Paginator\Paginator;
	use Zend\Paginator\Adapter\Iterator as paginatorIterator;

	class CustomerController extends AbstractActionController {
		protected $CustomerTable;
		protected $TaskTable;

		public function getCustomerTable() {
			if (!$this->CustomerTable) {
				$sm=$this->getServiceLocator();
				$this->CustomerTable=$sm->get('Application\Model\CustomerTable');
			}
			return $this->CustomerTable;
		}

		public function getTaskTable() {
			if (!$this->TaskTable) {
				$sm=$this->getServiceLocator();
				$this->TaskTable=$sm->get('Application\Model\TaskTable');
			}
			return $this->TaskTable;
		}

		public function customerAction() {
			$sessionManager=new SessionManager();
			$sessionManager->start();
			$userContainer=new Container('user');
			if (!$userContainer->offsetExists('islogged') or $userContainer->islogged!=true)
				$this->redirect()->toRoute('login');

			$view=new ViewModel();

			$page=$this->params()->fromRoute('page')?(int)$this->params()->fromRoute('page'):1;
			$list_customers=$this->getCustomerTable()->fetchAll();
			$itemsPerPage=10;
			$paginator=new Paginator(new paginatorIterator($list_customers));
			$paginator->setCurrentPageNumber($page)
					->setItemCountPerPage($itemsPerPage)
					->setPageRange(3);
			$list_tasks=$this->getTaskTable()->fetchAll();

			//instanciation du formulaire d'ajout d'un client
			$form_customer=new CustomerForm();
			//instanciation du filtre du formulaire
			$formFilter=new CustomerFormFilter();
			$form_customer->setInputFilter($formFilter);

			//vérification de l'entrée des données POST
			if ($this->getRequest()->isPost()) {
				$form_customer->setData($_POST);
				if ($form_customer->isValid()) {
					$postData=new Customer();
					$postData->id_customer			= $_POST['id_customer'];
					$postData->firstname			= $_POST['firstname'];
					$postData->lastname				= $_POST['lastname'];
					$postData->email 				= $_POST['email'];
					$postData->company_name			= $_POST['company_name'];
					$postData->phone				= $_POST['phone'];
					$postData->company_adress		= $_POST['company_adress'];
					$postData->company_city			= $_POST['company_city'];
					$postData->company_department	= $_POST['company_department'];

					$this->getCustomerTable()->saveCustomer($postData);
					$this->postRedirectGet('customer');
				}
			}

			return $view->setVariables(array(
				'list_customers'	=> $list_customers,
				'paginator' 		=> $paginator,
				'form_customer' 	=> $form_customer,
				'list_tasks'		=> $list_tasks
			));
		}
	}