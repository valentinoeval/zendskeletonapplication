<?php
	namespace Application\Model;

	class Note {
		/**
		 * 
		 * @var int
		 */
		public $id_note;
		/**
		 * 
		 * @var int
		 */
		public $id_customer;
		/**
		 * 
		 * @var string
		 */
		public $content;

		/**
		 * 
		 * @param unknown $datas
		 */
		public function exchangeArray($datas) {
			$this->id_note=(isset($datas['id_note']))?$datas['id_note']:null;
			$this->id_customer=(isset($datas['id_customer']))?$datas['id_customer']:null;
			$this->content=(isset($datas['content']))?$datas['content']:null;
		}
	}