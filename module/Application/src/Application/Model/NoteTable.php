<?php
	namespace Application\Model;

	use Zend\Db\Sql\Select;
	use Zend\Db\Adapter\Adapter;
	use Zend\Db\ResultSet\ResultSet;
	use Zend\Db\TableGateway\AbstractTableGateway;

	class NoteTable extends AbstractTableGateway {
		/**
		 * 
		 * @var string
		 */
		protected $table='notes';

		/**
		 * Constructeur
		 * @param Adapter $adapter
		 */
		public function __construct(Adapter $adapter) {
			$this->adapter=$adapter;
			$this->resultSetPrototype=new ResultSet();
			$this->resultSetPrototype->setArrayObjectPrototype(new Note());
			$this->initialize();
		}

		/**
		 * retourne toutes les tâches
		 * @return ResultSet
		 */
		public function fetchAll() {
			$resultSet=$this->select();
			return $resultSet;
		}

		/**
		 * retourne une tâche particulière par son id
		 * @param int $id_note
		 * @throws \Exception
		 * @return rowset
		 */
		public function getNote($id_note) {
			$id_note=(int)$id_note;
			$rowset=$this->select(array('id_note' => $id_note));
			if (!$rowset)
				throw new \Exception("Error Processing Request at getNote function");

			return $rowset;             
		}

		/**
		 * retourne toutes les tâches associées à un client
		 * @param int $id_customer
		 * @throws \Exception
		 * @return rowset
		 */
		public function getNoteByCustomer($id_customer) {
			$id_customer=(int)$id_customer;
			$rowset=$this->select(
				function(Select $select) use ($id_customer) {
					$select->where(array('id_customer' => $id_customer));
					$select->order('id_note DESC');
				}
			);
			if (!$rowset)
				throw new \Exception("Error Processing Request at getNoteByCustomer function");

			return $rowset;             
		}

		/**
		 * ajoute ou met à jour une note
		 * @param Note $note
		 * @throws \Exception
		 */
		public function saveNote(Note $note) {
			$datas=array(
				'id_note'       => $note->id_note,
				'id_customer'   => (int)$note->id_customer,
				'content'       => $note->content

			);
			$id_note=(int)$note->id_note;
			if ($id_note==0)
				$this->insert($datas);
			elseif ($this->getNote($id_note)) {
				$this->update($datas, array('id_note' => $id_note));
			}
			else
				throw new \Exception("Error Processing Request at saveNote function");
		}

		/**
		 * supprime une tâche
		 * @param int $id_note
		 */
		public function deleteNote($id_note) {
			$this->delete(array('id_note' => $id_note));
		}
	}