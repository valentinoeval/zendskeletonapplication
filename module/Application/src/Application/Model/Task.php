<?php
	namespace Application\Model;

	class Task {
		/**
		 * 
		 * @var int
		 */
		public $id_task;
		/**
		 * 
		 * @var int
		 */
		public $id_customer;
		/**
		 * 
		 * @var string
		 */
		public $content;
		/**
		 * 
		 * @var date
		 */
		public $exec_date;
		/**
		 * 
		 * @var int
		 */
		public $todo;

		public function exchangeArray($datas) {
			$this->id_task=(isset($datas['id_task']))?$datas['id_task']:null;
			$this->id_customer=(isset($datas['id_customer']))?$datas['id_customer']:null;
			$this->content=(isset($datas['content']))?$datas['content']:null;
			$this->exec_date=(isset($datas['exec_date']))?$datas['exec_date']:null;
			$this->todo=(isset($datas['todo']))?$datas['todo']:null;
		}
	}