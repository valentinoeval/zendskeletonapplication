<?php
	namespace Application\Model;
	
	use Zend\InputFilter\InputFilter;
	
	class CustomerFormFilter extends InputFilter {		
		public function __construct() {
			$filter=new InputFilter();
			
			$filter->add(array(
				'name'		=> 'firstname',
				'required'	=> true,
				'validators'=> array(
					array(
						'name'		=> 'StringLength',
						'options'	=> 	array(
							'encoding'	=> 'UTF-8',
							'min'		=> '2',
							'max'		=> '20'
						)
					)
				)
			));
			$filter->add(array(
				'name'		=> 'lastname',
				'required'	=> true,
				'validators'=> array(
					array(
						'name'		=> 'StringLength',
						'options'	=> 	array(
							'encoding'	=> 'UTF-8',
							'min'		=> '2',
							'max'		=> '20'
						)
					)
				)
			));
			$filter->add(array(
				'name'		=> 'email',
				'required'	=> true,
				'validators'=> array(
					array(
						'name'		=> 'StringLength',
						'options'	=> 	array(
							'encoding'	=> 'UTF-8',
							'min'		=> '2',
							'max'		=> '20'
						),
						array(
							'name' => 'EmailAdress'
						)
					)
				)
			));
			$filter->add(array(
				'name'		=> 'company_name',
				'required'	=> true,
				'validators'=> array(
					array(
						'name'		=> 'StringLength',
						'options'	=> 	array(
							'encoding'	=> 'UTF-8',
							'min'		=> '2',
							'max'		=> '20'
						)
					)
				)
			));
			$filter->add(array(
				'name'		=> 'phone',
				'required'	=> true,
				'validators'=> array(
					array(
						'name'		=> 'StringLength',
						'options'	=> 	array(
							'encoding'	=> 'UTF-8',
							'min'		=> '4',
							'max'		=> '10'
						)
					)
				)
			));
			$filter->add(array(
				'name'		=> 'company_adress',
				'required'	=> true,
				'validators'=> array(
					array(
						'name'		=> 'StringLength',
						'options'	=> 	array(
							'encoding'	=> 'UTF-8',
							'min'		=> '4',
							'max'		=> '100'
						)
					)
				)
			));
			$filter->add(array(
				'name'		=> 'company_city',
				'required'	=> true,
				'validators'=> array(
					array(
						'name'		=> 'StringLength',
						'options'	=> 	array(
							'encoding'	=> 'UTF-8',
							'min'		=> '2',
							'max'		=> '20'
						)
					)
				)
			));
			$filter->add(array(
				'name'		=> 'company_department',
				'required'	=> true,
				'validators'=> array(
					array(
						'name'		=> 'StringLength',
						'options'	=> 	array(
							'encoding'	=> 'UTF-8',
							'min'		=> '2',
							'max'		=> '20'
						)
					)
				)
			));
		}
	}