<?php
	namespace Application\Model;

class Login {
	/**
	 * 
	 * @var int
	 */
	public $id_user;
	/**
	 * 
	 * @var string
	 */
	public $login;
	/**
	 * 
	 * @var string
	 */
	public $password;

	/**
	 * 
	 * @param unknown $datas
	 */
	public function exchangeArray($datas) {
		$this->id_user=(isset($datas['id_user']))?$datas['id_user']:null;
		$this->login=(isset($datas['login']))?$datas['login']:null;
		$this->password=(isset($datas['password']))?$datas['password']:null;
	}
}