<?php
	namespace Application\Model;

	use Zend\Db\Sql\Select;
	use Zend\Db\Adapter\Adapter;
	use Zend\Db\ResultSet\ResultSet;
	use Zend\Db\TableGateway\AbstractTableGateway;

	class Logintable extends AbstractTableGateway {
		/**
		 * 
		 * @var string
		 */
		protected $table='users';
		
		/**
		 * Construteur
		 * @param Adapter $adapter
		 */
		public function __construct(Adapter $adapter) {
			$this->adapter=$adapter;
			$this->resultSetPrototype=new ResultSet();
			$this->resultSetPrototype->setArrayObjectPrototype(new Login());
			$this->initialize();
		}

		/**
		 * retourne toutes les inforations de l'utilisateur si celui ci existe
		 * @param string $login
		 * @throws \Exception
		 * @return rowset
		 */
		public function getLogin($login) {
			$rowset=$this->select(array('login' => $login));
			if (!$rowset) {
				throw new \Exception("Could not find row user");
			}
			return $rowset;
		}
	}