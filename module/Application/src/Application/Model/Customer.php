<?php
	namespace Application\Model;

	class Customer {
		/**
		 * 
		 * @var int
		 */
		public $id_customer;
		/**
		 * 
		 * @var string
		 */
		public $firstname;
		/**
		 * 
		 * @var string
		 */
		public $lastname;
		/**
		 * 
		 * @var string
		 */
		public $email;
		/**
		 * 
		 * @var string
		 */
		public $company_name;
		/**
		 * 
		 * @var string
		 */
		public $phone;
		/**
		 * 
		 * @var string
		 */
		public $company_adress;
		/**
		 * 
		 * @var string
		 */
		public $company_city;
		/**
		 * 
		 * @var string
		 */
		public $company_department;

		/**
		 * 
		 * @param unknown $datas
		 */
		public function exchangeArray($datas) {
			/*if (isset($datas['login']))
				$this->$login=$datas['login'];
			else
				$this->$login=null;

			OU*/
			$this->id_customer=(isset($datas['id_customer']))?$datas['id_customer']:null;
			$this->firstname=(isset($datas['firstname']))?$datas['firstname']:null;
			$this->lastname=(isset($datas['lastname']))?$datas['lastname']:null;
			$this->email=(isset($datas['email']))?$datas['email']:null;
			$this->company_name=(isset($datas['company_name']))?$datas['company_name']:null;
			$this->phone=(isset($datas['phone']))?$datas['phone']:null;
			$this->company_adress=(isset($datas['company_adress']))?$datas['company_adress']:null;
			$this->company_city=(isset($datas['company_city']))?$datas['company_city']:null;
			$this->company_department=(isset($datas['company_department']))?$datas['company_department']:null;
		}
	}