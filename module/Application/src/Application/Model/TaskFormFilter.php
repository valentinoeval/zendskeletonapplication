<?php
	namespace Application\Model;
	
	use Zend\InputFilter\InputFilter;
	
	class TaskFormFilter extends InputFilter {		
		public function __construct() {
			$filter=new InputFilter();

			$filter->add(array(
				'name'		=> 'id_customer',
				'required'	=> true,
				'validators'=> array(
					array(
						'name'		=> 'StringLength',
						'options'	=> 	array(
							'encoding'	=> 'UTF-8',
							'min'		=> '1'
						)
					)
				)
			));
			$filter->add(array(
				'name'		=> 'content',
				'required'	=> true,
				'validators'=> array(
					array(
						'name'		=> 'StringLength',
						'options'	=> 	array(
							'encoding'	=> 'UTF-8',
							'min'		=> '1',
							'max'		=> '1000'
						)
					)
				)
			));
			$filter->add(array(
				'name'		=> 'todo',
				'required'	=> true,
				'validators'=> array(
					array(
						'name'		=> 'StringLength',
						'options'	=> 	array(
							'encoding'	=> 'UTF-8',
							'min'		=> '1',
							'max'		=> '1'
						)
					)
				)
			));
		}
	}