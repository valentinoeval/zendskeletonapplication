<?php
	namespace Application\Model;

	use Zend\Db\Sql\Select;
	use Zend\Db\Adapter\Adapter;
	use Zend\Db\ResultSet\ResultSet;
	use Zend\Db\TableGateway\AbstractTableGateway;

	class TaskTable extends AbstractTableGateway {
		protected $table='tasks';

		public function __construct(Adapter $adapter) {
			$this->adapter=$adapter;
			$this->resultSetPrototype=new ResultSet();
			$this->resultSetPrototype->setArrayObjectPrototype(new Task());
			$this->initialize();
		}

		//retourne toutes les tâches
		public function fetchAll() {
			$resultSet=$this->select();
			return $resultSet;
		}

		//retourne une tâche particulière par son id
		public function getTask($id_task) {
			$id_task=(int)$id_task;
			$rowset=$this->select(array('id_task' => $id_task));
			if (!$rowset)
				throw new \Exception("Error Processing Request at getTask function");

			return $rowset;             
		}

		//retourne toutes les tâches associées à un client
		public function getTaskByCustomer($id_customer) {
			$id_customer=(int)$id_customer;
			$rowset=$this->select(array('id_customer' => $id_customer));
			if (!$rowset)
				throw new \Exception("Error Processing Request at getTaskByCustomer function");

			return $rowset;             
		}

		//ajoute ou met à jour une tâche
		public function saveTask(Task $task) {
			$datas=array(
				'id_task'       => $task->id_task,
				'id_customer'   => (int)$task->id_customer,
				'content'       => $task->content,
				'exec_date'     => $task->exec_date,
				'todo'          => $task->todo

			);
			$id_task=(int)$task->id_task;
			if ($id_task==0)
				$this->insert($datas);
			elseif ($this->getTask($id_task)) {
				$this->update($datas, array('id_task' => $id_task));
			}
			else
				throw new \Exception("Error Processing Request at saveTask function");
		}

		//supprime une tâche
		public function deleteTask($id_task) {
			$this->delete(array('id_task' => $id_task));
		}
	}