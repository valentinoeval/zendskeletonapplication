<?php
	namespace Application\Model;

	use Zend\Db\Sql\Select;
	use Zend\Db\Adapter\Adapter;
	use Zend\Db\ResultSet\ResultSet;
	use Zend\Db\TableGateway\AbstractTableGateway;

	class CustomerTable extends AbstractTableGateway {
		/**
		 * nom de la table dans la base
		 * @var string
		 */
		protected $table='customers';

		/**
		 * Construteur
		 * @param Adapter $adapter
		 */
		public function __construct(Adapter $adapter) {
			$this->adapter=$adapter;
			$this->resultSetPrototype=new ResultSet();
			$this->resultSetPrototype->setArrayObjectPrototype(new Customer());
			$this->initialize();
		}

		/**
		 * retourne tous les les utilisateur de l'application
		 * @return ResultSet
		 */
		public function fetchAll() {
			$select=new Select();
			$select->from($this->table);
			$resultSet=$this->selectWith($select);
			$resultSet->buffer();

			return $resultSet;
	}

		/**
		 * retourne un utilisateur précis de type Customer
		 * @param int $id_customer
		 * @throws \Exception
		 * @return rowset
		 */
		public function getCustomer($id_customer) {
			$id_customer=(int)$id_customer;
			$rowset=$this->select(array('id_customer' => $id_customer));
			//$row=$rowset->current(); -> uniquement pour les resultset
			if (!$rowset)
				throw new \Exception("Error Processing Request at getCustomer function");

			return $rowset;				
		}

		/**
		 * ajoute ou met à jour un client
		 * @param Customer $customer
		 * @throws \Exception
		 */
		public function saveCustomer(Customer $customer) {
			$datas=array(
				'id_customer'		=> $customer->id_customer,
				'firstname'			=> $customer->firstname,
				'lastname'			=> $customer->lastname,
				'email'				=> $customer->email,
				'company_name'		=> $customer->company_name,
				'phone'				=> $customer->phone,
				'company_adress'	=> $customer->company_adress,
				'company_city'		=> $customer->company_city,
				'company_department'=> $customer->company_department
			);
			$id_customer=(int)$customer->id_customer;
			if ($id_customer==0)
				$this->insert($datas);
			elseif ($this->getCustomer($id_customer)) {
				$this->update($datas, array('id_customer' => $id_customer));
			}
			else
				throw new \Exception("Error Processing Request at saveCustomer function");
		}

		/**
		 * supprime un utlisateur
		 * @param int $id_customer
		 */
		public function deleteCustomer($id_customer) {
			$this->delete(array('id_customer' => $id_customer));
		}
	}