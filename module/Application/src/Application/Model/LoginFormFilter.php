<?php
	namespace Application\Model;
	
	use Zend\InputFilter\InputFilter;
	
	class LoginFormFilter extends InputFilter {
		public function __construct() {
			$this->add(array(
				'name'		=> 'login',
				'required'	=> true,
				'validators'=> array(
					array(
						'name'		=> 'StringLength',
						'options'	=> 	array(
							'encoding'	=> 'UTF-8',
							'min'		=> '1',
							'max'		=> '20'
						),
					),
				)
			));
		}
	}